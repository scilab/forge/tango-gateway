/*!
 *  \file sci_tango_get_timeout.cxx
 *  \brief get client side timeout for device im ms
 *  \author muse
 *  \version 1.0
 *  \copyright 	This file is released under the 3-clause BSD license. See COPYING-BSD.
 */

#include <string>
#include <iostream> 
#include <tango.h>
#include <math.h>
#include <tango_connexion.h>

/*! \namespace std
 * standard namespace
 *
 *  \namespace Tango
 *  Tango namespace
 */

using namespace std;
using namespace Tango;

//extern C use to counter c++ mangling

extern "C" 
{

#include "api_scilab.h"
#include "Scierror.h"
#include "MALLOC.h"
#include <sciprint.h>


/*!
 * \fn int sci_tango_get_timeout(char *fname)
 * \brief function to get timeout from a device in ms
 * \param device_address : address of the Device Server
 * \exception Tango::DevFailed& Tango error
 * \returns client_timeout / tango_error
 */
int sci_tango_get_timeout(char *fname)
{
	SciErr sciErr;

	///device_address
	int m1 = 0, n1 = 0;
	int *piAddressVarOne = NULL;
	char *pStVarOne = NULL;
	int lenStVarOne = 0;
	int iType1 = 0;

	/// Check the number of input argument
	CheckInputArgument(pvApiCtx, 1,1);

	/// Check the number of output argument
	CheckOutputArgument(pvApiCtx, 1,2);

	/// get Address of inputs
	sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddressVarOne);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}


	/// checks types */
	sciErr = getVarType(pvApiCtx, piAddressVarOne, &iType1);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	if ( iType1 != sci_strings )
	{
		Scierror(999,"%s: Wrong type for input argument #%d: A string expected.\n",fname,1);
		return 0;
	}


	/// get string */
	/// string #1 : device address
	sciErr = getMatrixOfString(pvApiCtx, piAddressVarOne,&m1,&n1,&lenStVarOne,&pStVarOne);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}


	if ( (m1 != n1) && (n1 != 1) )
	{
		Scierror(999,"%s: Wrong size for input argument #%d: A string expected.\n",fname,1);
		return 0;
	}

	pStVarOne = (char*)MALLOC(sizeof(char)*(lenStVarOne + 1));

	sciErr = getMatrixOfString(pvApiCtx, piAddressVarOne,&m1,&n1,&lenStVarOne,&pStVarOne);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}


	///declaration of the ouput (matrix/time/quality/size of/tango error)

	double tango_error = 0;
	int error_severity = 0;
	double client_timeout = 0;
	try
	{
		///connect to the DevProxy
		DeviceProxy *dev = tango_connexion(pStVarOne);
		///get the client timeout
		client_timeout=(double)dev->get_timeout_millis();
		
	}

	catch (DevFailed& df)
	{
		///build the error message
		string error = "Tango_Error :\nReason :";
		for (int err = 0; err < df.errors.length(); err++)
		{
			error = error + df.errors[err].reason.in();
			error = error + "\n";
		}

		error = error + "\nDescription : \n";
		for (int err = 0; err < df.errors.length(); err++)
		{
			error = error + df.errors[err].desc.in();
			error = error + "\n";
		}
		error = error + "\nOrigin : \n";
		for (int err = 0; err < df.errors.length(); err++)
		{
			error = error + df.errors[err].origin.in();
			error = error + "\n";
		}

		for (int err = 0; err < df.errors.length(); err++)
		{
			error_severity = df.errors[err].severity;
		}
		error = error +"\nSeverity : ";
		switch(error_severity)
		{
		case Tango::WARN :
			error = error + "WARN";
			break;

		case Tango::ERR :
			error = error +"ERROR";
			break;

		case Tango::PANIC :
			error = error + "PANIC";
			break;
		default :
			error = error + "Unknown severity code";
			break;
		}
		Scierror(2500, error.c_str());
		free(pStVarOne);
		tango_error=-1;
		return 0;
	}

	/// create result on stack : size of time_stamp and quality variable
	int m_out = 1;
	int n_out = 1;

	///tango_error
	createMatrixOfDouble(pvApiCtx, nbInputArgument(pvApiCtx) + 1, m_out, n_out, &client_timeout);
	AssignOutputVariable(pvApiCtx, 1) = nbInputArgument(pvApiCtx) + 1;

	createMatrixOfDouble(pvApiCtx, nbInputArgument(pvApiCtx) + 2, m_out, n_out, &tango_error);
	AssignOutputVariable(pvApiCtx, 2) = nbInputArgument(pvApiCtx) + 2;


	///Free memory
	free(pStVarOne);

	return 0;
}

}
/* ==================================================================== */
