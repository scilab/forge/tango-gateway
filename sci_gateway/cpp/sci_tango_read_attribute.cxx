/*!
 *  \file sci_tango_read_attribute.cxx
 *  \brief Read attribute from a Tango Device Server
 *  \author muse
 *  \version 1.0
 *  \copyright 	This file is released under the 3-clause BSD license. See COPYING-BSD.
 */

#include <string>
#include <iostream> 
#include <tango.h>
#include <math.h>
#include <tango_device_attribute.h>
#include <tango_connexion.h>

/*! \namespace std
 * standard namespace
 *
 *  \namespace Tango
 *  Tango namespace
 */

using namespace std;
using namespace Tango;

//extern C use to counter c++ mangling

extern "C" 
{

#include "api_scilab.h"
#include "Scierror.h"
#include "MALLOC.h"
#include <sciprint.h>


/*!
 * \fn int sci_tango_read_attribute(char *fname)
 * \brief function called to read a Device Attribute from a Device Server
 * \param device_address : address of the Device Server
 * \param attribute_name : name of the attribute read
 * \exception Tango::DevFailed& Tango error
 * \returns attribute_value : value of the attribute read / tango_error / time_stamp / quality_factor
 */
int sci_tango_read_attribute(char *fname)
{
	SciErr sciErr;

	///device_address
	int m1 = 0, n1 = 0;
	int *piAddressVarOne = NULL;
	char *pStVarOne = NULL;
	int lenStVarOne = 0;
	int iType1 = 0;

	/// attribute_name
	int m2 = 0, n2 = 0;
	int *piAddressVarTwo = NULL;
	char *pStVarTwo = NULL;
	int lenStVarTwo = 0;
	int iType2 = 0;

	/// Check the number of input argument
	CheckInputArgument(pvApiCtx, 2,2);

	/// Check the number of output argument
	CheckOutputArgument(pvApiCtx, 1,4);

	/// get Address of inputs
	sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddressVarOne);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}
	sciErr = getVarAddressFromPosition(pvApiCtx, 2, &piAddressVarTwo);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	/// checks types */
	sciErr = getVarType(pvApiCtx, piAddressVarOne, &iType1);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	if ( iType1 != sci_strings )
	{
		Scierror(999,"%s: Wrong type for input argument #%d: A string expected.\n",fname,1);
		return 0;
	}

	sciErr = getVarType(pvApiCtx, piAddressVarTwo, &iType2);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	if ( iType2 != sci_strings )
	{
		Scierror(999,"%s: Wrong type for input argument #%d: A string expected.\n",fname,2);
		return 0;
	}

	/// get strings */
	/// string #1 : device address
	sciErr = getMatrixOfString(pvApiCtx, piAddressVarOne,&m1,&n1,&lenStVarOne,&pStVarOne);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}


	if ( (m1 != n1) && (n1 != 1) )
	{
		Scierror(999,"%s: Wrong size for input argument #%d: A string expected.\n",fname,1);
		return 0;
	}

	pStVarOne = (char*)MALLOC(sizeof(char)*(lenStVarOne + 1));

	sciErr = getMatrixOfString(pvApiCtx, piAddressVarOne,&m1,&n1,&lenStVarOne,&pStVarOne);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	/// string #2 : attribute name
	sciErr = getMatrixOfString(pvApiCtx, piAddressVarTwo,&m2,&n2,&lenStVarTwo,&pStVarTwo);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	if ( (m2 != n2) && (n2 != 1) )
	{
		Scierror(999,"%s: Wrong size for input argument #%d: A string expected.\n",fname,2);
		return 0;
	}

	pStVarTwo = (char*)MALLOC(sizeof(char)*(lenStVarTwo + 1));

	sciErr = getMatrixOfString(pvApiCtx, piAddressVarTwo,&m2,&n2,&lenStVarTwo,&pStVarTwo);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}
	///declaration of the ouput (matrix/time/quality/size of/tango error)

	double * double_data = NULL;
	double attr_time_stamp = NULL;
	double attr_quality = NULL;
	double tango_error = 0;
	int m1_out = 1; 
	int n1_out = 1;
	int error_severity = 0;
	int data_type = NULL;
	char ** char_data = NULL;

	try
	{
		///connect to the DevProxy
		DeviceProxy *dev = tango_connexion(pStVarOne);
		///read the DevAttr
		DeviceAttribute attr_data=dev->read_attribute(pStVarTwo);
		///resize the ouput
		m1_out = attr_data.get_dim_x();
		if(attr_data.get_data_format()==Tango::IMAGE)
		{
			n1_out = attr_data.get_dim_y();
		}
		else
		{
			n1_out = attr_data.get_dim_y()+1;
		}
		///convert DevAttr in double and assign as output #1
		data_type = attr_data.get_type();
		switch(data_type)
		{
		/*! \bug case Tango::DEV_STRING : need to be fix to work with string
			{
			char_data = tango_convert_attribute_to_double(attr_data, data_type);
			createMatrixOfDouble(pvApiCtx, nbInputArgument(pvApiCtx) + 1, m_out, n_out, char_data);
			AssignOutputVariable(pvApiCtx, 2) = nbInputArgument(pvApiCtx) + 1;
			break;			
			}*/
		default :
		{
			double_data = tango_convert_attribute_to_double(attr_data, data_type);
			createMatrixOfDouble(pvApiCtx, nbInputArgument(pvApiCtx) + 1, m1_out, n1_out, double_data);
			AssignOutputVariable(pvApiCtx, 1) = nbInputArgument(pvApiCtx) + 1;

		}
		}


		///catch quality factor / timestamp
		attr_time_stamp = (double)attr_data.get_date().tv_sec + ((double)attr_data.get_date().tv_usec/pow(10,6)) + ((double)attr_data.get_date().tv_nsec/pow(10,9));

		attr_quality = (double)attr_data.get_quality();



	}

	catch (DevFailed& df)
	{
		///build the error message
		string error = "Tango_Error :\nReason :";
		for (int err = 0; err < df.errors.length(); err++)
		{
			error = error + df.errors[err].reason.in();
			error = error + "\n";
		}

		error = error + "\nDescription : \n";
		for (int err = 0; err < df.errors.length(); err++)
		{
			error = error + df.errors[err].desc.in();
			error = error + "\n";
		}
		error = error + "\nOrigin : \n";
		for (int err = 0; err < df.errors.length(); err++)
		{
			error = error + df.errors[err].origin.in();
			error = error + "\n";
		}

		for (int err = 0; err < df.errors.length(); err++)
		{
			error_severity = df.errors[err].severity;
		}
		error = error +"\nSeverity : ";
		switch(error_severity)
		{
		case Tango::WARN :
			error = error + "WARN";
			break;

		case Tango::ERR :
			error = error +"ERROR";
			break;

		case Tango::PANIC :
			error = error + "PANIC";
			break;
		default :
			error = error + "Unknown severity code";
			break;
		}
		Scierror(2500, error.c_str());
		free(double_data);
		free(pStVarTwo);
		free(pStVarOne);
		tango_error=-1;
		return 0;
	}

	/// create result on stack : size of time_stamp and quality variable
	int m_out = 1;
	int n_out = 1;

	///tango_error
	createMatrixOfDouble(pvApiCtx, nbInputArgument(pvApiCtx) + 2, m_out, n_out, &tango_error);
	AssignOutputVariable(pvApiCtx, 2) = nbInputArgument(pvApiCtx) + 2;

	///time_stamp
	createMatrixOfDouble(pvApiCtx, nbInputArgument(pvApiCtx) + 3, m_out, n_out, &attr_time_stamp);
	AssignOutputVariable(pvApiCtx, 3) = nbInputArgument(pvApiCtx) + 3;

	///quality factor
	createMatrixOfDouble(pvApiCtx, nbInputArgument(pvApiCtx) + 4, m_out, n_out, &attr_quality);
	AssignOutputVariable(pvApiCtx, 4) = nbInputArgument(pvApiCtx) + 4;

	///Free memory
	free(char_data);
	free(double_data);
	free(pStVarTwo);
	free(pStVarOne);

	return 0;
}

}
/* ==================================================================== */
