/*!
 *  \file sci_tango_write_attribute.cxx
 *  \brief write attribute in a Tango Device Server
 *  \author muse
 *  \version 1.0
 */

#include <string>
#include <iostream> 
#include <tango.h>
#include <math.h>
#include <tango_device_attribute.h>
#include <tango_connexion.h>

/*! \namespace std
 * standard namespace
 *
 *  \namespace Tango
 *  Tango namespace
 */

using namespace std;
using namespace Tango;
/// extern C use to counter C++ mangling
extern "C" 
{

#include "api_scilab.h"
#include "Scierror.h"
#include "MALLOC.h"
#include <sciprint.h>

/*!
 * \fn int sci_tango_write_attribute(char *fname)
 * \brief function called to write a Device Attribute in a Device Server
 * \param device_address : address of the Device Server
 * \param attribute_name : name of the attribute written
 * \param attribute_value : value of the attribute written
 * \return attribute_value : value of the attribute read
 */
int sci_tango_write_attribute(char *fname)
{
	SciErr sciErr;

	int m1 = 0, n1 = 0;
	int *piAddressVarOne = NULL;
	char *pStVarOne = NULL;
	int lenStVarOne = 0;
	int iType1 = 0;

	int m2 = 0, n2 = 0;
	int *piAddressVarTwo = NULL;
	char *pStVarTwo = NULL;
	int lenStVarTwo = 0;
	int iType2 = 0;

	int m3 = 0, n3 = 0;
	int *piAddressVarThree = NULL;
	double * pStVarThree = NULL;
	int iType3 = 0;

	///Check the number of input argument
	CheckInputArgument(pvApiCtx, 3,3);

	///Check the number of output argument
	CheckOutputArgument(pvApiCtx, 0,1);

	///get Address of inputs
	sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddressVarOne);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}
	sciErr = getVarAddressFromPosition(pvApiCtx, 2, &piAddressVarTwo);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}
	sciErr = getVarAddressFromPosition(pvApiCtx, 3, &piAddressVarThree);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	///checks types
	sciErr = getVarType(pvApiCtx, piAddressVarOne, &iType1);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	if ( iType1 != sci_strings )
	{
		Scierror(999,"%s: Wrong type for input argument #%d: A string expected.\n",fname,1);
		return 0;
	}

	sciErr = getVarType(pvApiCtx, piAddressVarTwo, &iType2);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	if ( iType2 != sci_strings )
	{
		Scierror(999,"%s: Wrong type for input argument #%d: A string expected.\n",fname,2);
		return 0;
	}

	sciErr = getVarType(pvApiCtx, piAddressVarThree, &iType3);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	if ( iType3 != sci_matrix )
	{
		Scierror(999,"%s: Wrong type for input argument #%d: A scalar expected.\n",fname,2);
		return 0;
	}
	///get inputs

	///String One
	sciErr = getMatrixOfString(pvApiCtx, piAddressVarOne,&m1,&n1,&lenStVarOne,&pStVarOne);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	///check size string One
	if ( (m1 != n1) && (n1 != 1) )
	{
		Scierror(999,"%s: Wrong size for input argument #%d: A string expected.\n",fname,1);
		return 0;
	}
	///alloc string  One
	pStVarOne = (char*)MALLOC(sizeof(char)*(lenStVarOne + 1));
	///get string One
	sciErr = getMatrixOfString(pvApiCtx, piAddressVarOne,&m1,&n1,&lenStVarOne,&pStVarOne);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}
	///String Two
	sciErr = getMatrixOfString(pvApiCtx, piAddressVarTwo,&m2,&n2,&lenStVarTwo,&pStVarTwo);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}
	///check size string Two
	if ( (m2 != n2) && (n2 != 1) )
	{
		Scierror(999,"%s: Wrong size for input argument #%d: A string expected.\n",fname,2);
		return 0;
	}
	///alloc string
	pStVarTwo = (char*)MALLOC(sizeof(char)*(lenStVarTwo + 1));
	///get string Two
	sciErr = getMatrixOfString(pvApiCtx, piAddressVarTwo,&m2,&n2,&lenStVarTwo,&pStVarTwo);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	///Double three (value of the attribute to write as a simple scalar
	sciErr = getMatrixOfDouble(pvApiCtx, piAddressVarThree,&m3,&n3,&pStVarThree);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	///end of get inputs

	///declaration of the ouput (tango_error) & the error severity to build the error message
	double tango_error = 0;
	int error_severity = 0;
	try
	{
		///connect to the DevProxy
		DeviceProxy *dev = tango_connexion(pStVarOne);
		///init de DevAttr and convert as the format needed
		DeviceAttribute dev_attr;
		tango_convert_double_to_device_attribute(pStVarThree, pStVarTwo, dev, m3, n3, dev_attr);
		///write the DevAttr
		dev->write_attribute(dev_attr);

	}

	catch (DevFailed& df)
	{
		///build the error message
		string error = "Tango_Error :\nReason :";
		for (int err = 0; err < df.errors.length(); err++)
		{
			error = error + df.errors[err].reason.in();
			error = error + "\n";
		}

		error = error + "\nDescription : \n";
		for (int err = 0; err < df.errors.length(); err++)
		{
			error = error + df.errors[err].desc.in();
			error = error + "\n";
		}
		error = error + "\nOrigin : \n";
		for (int err = 0; err < df.errors.length(); err++)
		{
			error = error + df.errors[err].origin.in();
			error = error + "\n";
		}

		for (int err = 0; err < df.errors.length(); err++)
		{
			error_severity = df.errors[err].severity;
		}
		error = error +"\nSeverity : ";
		switch(error_severity)
		{
		case Tango::WARN :
			error = error + "WARN";
			break;

		case Tango::ERR :
			error = error +"ERROR";
			break;

		case Tango::PANIC :
			error = error + "PANIC";
			break;
		default :
			error = error + "Unknown severity code";
			break;
		}
		Scierror(2500, error.c_str());
		free(pStVarTwo);
		free(pStVarOne);
		tango_error=-1;
		return 0;
	}
	///assign output
	createMatrixOfDouble(pvApiCtx, nbInputArgument(pvApiCtx) + 1, 1, 1, &tango_error);
	AssignOutputVariable(pvApiCtx, 1) = nbInputArgument(pvApiCtx) + 1;

	free(pStVarTwo);
	free(pStVarOne);

	return 0;
}

}
