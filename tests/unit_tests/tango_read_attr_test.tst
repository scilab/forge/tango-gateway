
		//Test read attr with scalar variable

// bool
[a,b] = tango_read_attribute("sys/tg_test/1","boolean_scalar");
assert_checkequal(b , 0);

// short
[a,b]  = tango_read_attribute("sys/tg_test/1","short_scalar");
assert_checkequal(b , 0);

// ushort
[a,b]  = tango_read_attribute("sys/tg_test/1","ushort_scalar");
assert_checkequal(b , 0);

// long
[a,b]  = tango_read_attribute("sys/tg_test/1","long_scalar");
assert_checkequal(b , 0);

// ulong
[a,b]  = tango_read_attribute("sys/tg_test/1","ulong_scalar");
assert_checkequal(b , 0);

// float
[a,b]  = tango_read_attribute("sys/tg_test/1","float_scalar");
assert_checkequal(b , 0);

// double 
[a,b]  = tango_read_attribute("sys/tg_test/1","double_scalar");
assert_checkequal(b , 0);

// state
[a,b]  = tango_read_attribute("sys/tg_test/1","state");
assert_checkequal(b , 0);

		//Test read attr with spectrum variable

// bool
[a,b] = tango_read_attribute("sys/tg_test/1","boolean_spectrum");
assert_checkequal(b , 0);

// short
[a,b]  = tango_read_attribute("sys/tg_test/1","short_spectrum");
assert_checkequal(b , 0);

// ushort
[a,b]  = tango_read_attribute("sys/tg_test/1","ushort_spectrum");
assert_checkequal(b , 0);

// long
[a,b]  = tango_read_attribute("sys/tg_test/1","long_spectrum");
assert_checkequal(b , 0);

// ulong
[a,b]  = tango_read_attribute("sys/tg_test/1","ulong_spectrum_ro");
assert_checkequal(b , 0);

// float
[a,b]  = tango_read_attribute("sys/tg_test/1","float_spectrum");
assert_checkequal(b , 0);

// double 
[a,b]  = tango_read_attribute("sys/tg_test/1","double_spectrum");
assert_checkequal(b , 0);

		//Test read attr with image variable


// short
[a,b]  = tango_read_attribute("sys/tg_test/1","short_image");
assert_checkequal(b , 0);

// ushort
[a,b]  = tango_read_attribute("sys/tg_test/1","ushort_image");
assert_checkequal(b , 0);

// long
[a,b]  = tango_read_attribute("sys/tg_test/1","long_image");
assert_checkequal(b , 0);

// ulong
[a,b]  = tango_read_attribute("sys/tg_test/1","ulong_image_ro");
assert_checkequal(b , 0);

// float
[a,b]  = tango_read_attribute("sys/tg_test/1","float_image");
assert_checkequal(b , 0);

// double 
[a,b]  = tango_read_attribute("sys/tg_test/1","double_image");
assert_checkequal(b , 0);

