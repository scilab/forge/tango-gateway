// ====================================================================
// Andy Gotz + Martin Use - ESRF - 2013
// ====================================================================
function builder_cpp()
    //files_src_cpp = [
    //    "tango_convert_attribute_to_double"];

string TANGO_ROOT;
TANGO_ROOT=getenv("TANGO_ROOT");
    src_cpp_path = get_absolute_file_path('builder_cpp.sce');
    CFLAGS = "-g -D_DEBUG -D_REENTRANT -W -I"+TANGO_ROOT+"/include/tango -I/"+ src_cpp_path+" -std=c++0x -Dlinux";
    LDFLAGS = "-ltango -llog4tango -lCOS4 -lomniORB4 -lomniDynamic4 -lomnithread -lzmq -ldl -lpthread -lstdc++ ";

    tbx_build_src(["tango_src"], ..
                    ["tango_connexion.cpp","tango_device_attribute.cpp", "tango_device_data.cpp"], ..
		             "cpp", ..
                    src_cpp_path, ..
                    [""], ..
                    ["-ltango -llog4tango -lCOS4 -lomniORB4 -lomniDynamic4 -lomnithread -lzmq -ldl -lpthread -lstdc++"],..
                    [CFLAGS]);
    //unix("cp "+src_cpp_path+"/libtango_convert.so /usr/local/lib")

endfunction
// ====================================================================
builder_cpp();
clear builder_cpp;
// ====================================================================
